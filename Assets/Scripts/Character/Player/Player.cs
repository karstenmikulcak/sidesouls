using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WickedGames
{
    public class Player : ACharacter
    {
        private LocomotionState _locomotionState;
        private RotationState _rotationState;

        protected override Type[] StatesRequired =>
            new[] { typeof(LocomotionState), typeof(RotationState)};

        protected override void OnAwake()
        {
            _locomotionState = StateHolder.TryGetState<LocomotionState>();
            _rotationState = StateHolder.TryGetState<RotationState>();
        }

        internal void UpdateInput(CharacterInput input)
        {
            _rotationState.UpdateRotation(input.Movement);
            _locomotionState.UpdateDirection(input.Movement);
            _locomotionState.UpdateIsRunning(input.Run);
        }

        protected override void SubscribeToStates()
        {
        }

        protected override void UnSubscribeToStates()
        {
        }
    }
}
