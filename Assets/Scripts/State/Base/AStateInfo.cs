﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace WickedGames
{
    public abstract class AStateInfo : ScriptableObject
    {
        public abstract Type StateType { get; }

        public abstract AState CreateNewState();
    }
}
