﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace WickedGames
{

    public class VirtualInputManager : AManager<VirtualInputManager>
    {
        [ShowInInspector][ReadOnly] public CharacterInput PlayerInput { get; private set; }

        private void Awake()
        {
            PlayerInput = new CharacterInput(Vector2.zero, false, false);
        }
    }

    [Serializable]
    public class CharacterInput
    {
        public CharacterInput(Vector2 movement, bool jump, bool run)
        {
            Movement = movement;
            Run = run;
            Jump = jump;
        }

        [ShowInInspector] public Vector2 Movement { get; set; }
        [ShowInInspector] public bool Run { get; set; }
        [ShowInInspector] public bool Jump { get; set; }
    }
}
