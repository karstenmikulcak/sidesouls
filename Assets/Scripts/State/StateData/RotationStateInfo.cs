﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace WickedGames
{
    [CreateAssetMenu(fileName = "RotationStateInfo_", menuName = "StateHolder/StateInfo/Rotation")]
    public class RotationStateInfo : AStateInfo
    {
        public float RotationSpeed;
        public float RotationAnimationSpeed;
        public bool IsFacingForward;

        public override Type StateType => typeof(RotationState);

        public override AState CreateNewState()
        {
            return new RotationState(this);
        }
    }
}
