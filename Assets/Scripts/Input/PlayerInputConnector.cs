using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WickedGames
{
    public class PlayerInputConnector : MonoBehaviour
    {
        private Player _player;
        private VirtualInputManager _virtualInputManager;
        private GameManager _gameManager;

        private void Awake()
        {
            _virtualInputManager = VirtualInputManager.Instance;
            _gameManager = GameManager.Instance;
        }

        private void Start()
        {
            _player = _gameManager.Player;
        }

        private void OnEnable()
        {
            _gameManager.OnPlayerJoinedCallback += PlayerJoined;
        }

        private void OnDisable()
        {
            _gameManager.OnPlayerJoinedCallback -= PlayerJoined;
        }

        private void Update()
        {
            if (_player != null)
                _player.UpdateInput(_virtualInputManager.PlayerInput);
        }

        private void PlayerJoined(Player player)
        {
            _player = player;
        }
    }
}
