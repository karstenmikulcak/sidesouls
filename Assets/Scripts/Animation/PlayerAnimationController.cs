using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WickedGames
{
    public class PlayerAnimationController : StateConnector
    {
        private enum AnimationParameters
        {
            IsIdle,
            IsRunning,
            IsRotating
        }

        private Animator _animator;
        private LocomotionState _locomotionState;
        private RotationState _rotationState;

        protected override Type[] StatesRequired => new[] { typeof(LocomotionState), typeof(RotationState) };

        protected override void OnAwake()
        {
            _animator = GetComponent<Animator>();
            _locomotionState = StateHolder.TryGetState<LocomotionState>();
            _rotationState = StateHolder.TryGetState<RotationState>();
        }

        private void HandleIdle(bool isIdle)
        {
            _animator.SetBool(AnimationParameters.IsIdle.ToString(), isIdle);
        }

        private void HandleRunning(bool isRunning)
        {
            _animator.SetBool(AnimationParameters.IsRunning.ToString(), isRunning);
        }

        private void HandleRotating(bool isRotating)
        {
            _animator.SetBool(AnimationParameters.IsRotating.ToString(), isRotating);
        }

        protected override void SubscribeToStates()
        {
            _locomotionState.OnLocomotionStateSpeedChangeCallback += HandleRunning;
            _locomotionState.OnLocomotionStateIdleChangeCallback += HandleIdle;
            _rotationState.OnRotationStateIsRotatingChangeCallback += HandleRotating;
        }

        protected override void UnSubscribeToStates()
        {
            _locomotionState.OnLocomotionStateSpeedChangeCallback -= HandleRunning;
            _locomotionState.OnLocomotionStateIdleChangeCallback -= HandleIdle;
            _rotationState.OnRotationStateIsRotatingChangeCallback -= HandleRotating;
        }
    }
}
