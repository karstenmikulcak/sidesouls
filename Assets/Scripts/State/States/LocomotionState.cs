﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace WickedGames
{
    [Serializable]
    public class LocomotionState : AState
    {
        private bool _canMove;
        private Vector2 _currentDirection;
        private float _currentSpeed;
        private bool _isIdle;
        private bool _isRunning;
        private float _runSpeed;
        private float _walkSpeed;

        [HideInInspector]
        public delegate void OnLocomotionStateCanMoveChange(bool canMove);
        [HideInInspector]
        public delegate void OnLocomotionStateDirectionChange(Vector2 direction);
        [HideInInspector]
        public delegate void OnLocomotionStateIdleChange(bool isIdle);
        [HideInInspector]
        public delegate void OnLocomotionStateSpeedChange(bool isRunning);
        [HideInInspector]
        public delegate void OnMovementSpeedChange(float movementSpeed);

        [HideInInspector] 
        public OnLocomotionStateCanMoveChange OnLocomotionStateCanMoveChangeCallback;
        [HideInInspector] 
        public OnLocomotionStateDirectionChange OnLocomotionStateDirectionChangeCallback;
        [HideInInspector] 
        public OnLocomotionStateIdleChange OnLocomotionStateIdleChangeCallback;
        [HideInInspector] 
        public OnLocomotionStateSpeedChange OnLocomotionStateSpeedChangeCallback;
        [HideInInspector] 
        public OnMovementSpeedChange OnMovementSpeedChangeCallback;

        public LocomotionState(LocomotionStateInfo locomotionInfo)
        {
            AddForward = locomotionInfo.AddForward;

            _walkSpeed = locomotionInfo.WalkSpeed;
            _runSpeed = locomotionInfo.RunSpeed;
            _currentSpeed = _walkSpeed;
            _isRunning = false;
            _isIdle = true;
            _canMove = true;
        }

        [ShowInInspector]
        public bool AddForward { get; private set; }

            [ShowInInspector]
        public bool IsRunning
        {
            get => _isRunning;
            private set
            {
                _isRunning = value;
                OnLocomotionStateSpeedChangeCallback?.Invoke(_isRunning);
            }
        }

        [ShowInInspector]
        public Vector3 CurrentMoveForce
        {
            get => _currentDirection;
            private set
            {
                _currentDirection = value;
                OnLocomotionStateDirectionChangeCallback?.Invoke(_currentDirection);
            }
        }

        [ShowInInspector]
        public float CurrentMovementSpeed
        {
            get => _currentSpeed;
            private set
            {
                _currentSpeed = value;
                OnMovementSpeedChangeCallback?.Invoke(_currentSpeed);
            }
        }

        [ShowInInspector]
        public bool IsIdle
        {
            get => _isIdle;
            private set
            {
                _isIdle = value;
                OnLocomotionStateIdleChangeCallback?.Invoke(_isIdle);
            }
        }

        [ShowInInspector]
        public bool CanMove
        {
            get => _canMove;
            private set
            {
                _canMove = value;
                OnLocomotionStateCanMoveChangeCallback?.Invoke(_canMove);
            }
        }

        public void UpdateDirection(Vector2 direction)
        {
            if (_currentDirection == Vector2.zero && direction == Vector2.zero) return;

            CurrentMoveForce = direction;
            UpdateIsIdle(direction == Vector2.zero);
        }

        public void UpdateIsRunning(bool isRunning)
        {
            if (_isRunning == isRunning) return;
            IsRunning = isRunning;
            CurrentMovementSpeed = IsRunning ? _runSpeed : _walkSpeed;
        }

        public void UpdateIsIdle(bool isIdle)
        {
            if (_isIdle != isIdle) IsIdle = isIdle;
        }

        public void UpdateCanMove(bool canMove)
        {
            if (_canMove != canMove) CanMove = canMove;
        }

        public void IncreaseWalkSpeed(float amount)
        {
            var newAmount = _walkSpeed + amount;
            ChangeWalkSpeed(newAmount);
        }

        public void DecreaseWalkSpeed(float amount)
        {
            var newAmount = _walkSpeed - amount;
            ChangeWalkSpeed(newAmount);
        }

        public void IncreaseRunSpeed(float amount)
        {
            var newAmount = _runSpeed + amount;
            ChangeRunSpeed(newAmount);
        }

        public void DecreaseRunSpeed(float amount)
        {
            var newAmount = _runSpeed - amount;
            ChangeRunSpeed(newAmount);
        }

        private void ChangeWalkSpeed(float amount)
        {
            if (_walkSpeed == amount) return;
            _walkSpeed = amount;
            if (!IsRunning) CurrentMovementSpeed = amount;
        }

        private void ChangeRunSpeed(float amount)
        {
            if (_runSpeed == amount) return;
            _runSpeed = amount;
            if (IsRunning) CurrentMovementSpeed = amount;
        }

        public override void InvokeAll()
        {
            OnLocomotionStateSpeedChangeCallback?.Invoke(IsRunning);
            OnLocomotionStateIdleChangeCallback?.Invoke(IsIdle);
            OnLocomotionStateCanMoveChangeCallback?.Invoke(CanMove);
            OnMovementSpeedChangeCallback?.Invoke(CurrentMovementSpeed);
        }
    }
}