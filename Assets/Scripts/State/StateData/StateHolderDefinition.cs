﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace WickedGames
{
    [CreateAssetMenu(fileName = "StateHolderDefinition_", menuName = "StateHolder/Definition")]
    public class StateHolderDefinition : ScriptableObject
    {
        public StateHolderInfo Info;

        public List<AStateInfo> StateInfos;
    }
}
