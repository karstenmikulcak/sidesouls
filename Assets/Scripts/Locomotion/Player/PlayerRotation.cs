using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace WickedGames
{
    public class PlayerRotation : StateConnector
    {
        private const float ROTATION_SPEED_MULTIPLIER = 100;

        private float _speed;
        private IEnumerator _rotationTask;

        [SerializeField] private Transform _rotationTransform;
        [ReadOnly][ShowInInspector] private RotationState _rotationState;

        protected override Type[] StatesRequired => new[] { typeof(RotationState) };

        protected override void OnAwake()
        {
            _rotationState = StateHolder.TryGetState<RotationState>();
        }

        private void HandleRotationSpeed(float rotationSpeed)
        {
            _speed = rotationSpeed * ROTATION_SPEED_MULTIPLIER;
        }

        private void HandleRotation(bool isFacingForward)
        {
            _rotationState.UpdateIsRotating(true);

            var targetRotation = Quaternion.LookRotation(-_rotationTransform.forward,
            Vector3.up);

            if(_rotationTask != null) StopCoroutine(_rotationTask);
            _rotationTask = Rotate(targetRotation);
            StartCoroutine(_rotationTask);
        }

        IEnumerator Rotate(Quaternion targetRotation)
        {
            yield return new WaitForEndOfFrame();
            _rotationTransform.rotation = Quaternion.Slerp(_rotationTransform.rotation, targetRotation, _speed * Time.deltaTime);
            yield return new WaitForSeconds(_rotationState.CurrentAnimationRotationSpeed);
            _rotationTask = null;
            _rotationState.UpdateIsRotating(false);
        }

        protected override void SubscribeToStates()
        {
            _rotationState.OnRotationStateDirectionChangeCallback += HandleRotation;
            _rotationState.OnRotationStateSpeedChangeCallback += HandleRotationSpeed;
        }

        protected override void UnSubscribeToStates()
        {
            _rotationState.OnRotationStateDirectionChangeCallback -= HandleRotation;
            _rotationState.OnRotationStateSpeedChangeCallback -= HandleRotationSpeed;
        }
    }
}
