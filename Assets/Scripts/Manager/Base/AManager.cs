﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace WickedGames
{
    public abstract class AManager<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static bool _shuttingDown;
        private static readonly object _lock = new object();
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_shuttingDown)
                {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                                     "' already destroyed. Returning null.");
                    _instance = null;
                }

                lock (_lock)
                {
                    if (_instance != null) return _instance;
                    _instance = (T)FindObjectOfType(typeof(T));

                    if (_instance != null) return _instance;
                    var singletonObject = new GameObject();
                    _instance = singletonObject.AddComponent<T>();
                    singletonObject.name = typeof(T) + " (Singleton)";

                    DontDestroyOnLoad(singletonObject);

                    return _instance;
                }
            }
        }


        private void OnApplicationQuit()
        {
            _shuttingDown = true;
        }


        private void OnDestroy()
        {
            _shuttingDown = true;
        }
    }
}
