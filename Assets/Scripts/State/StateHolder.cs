using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace WickedGames
{
    public class StateHolder : MonoBehaviour
    {
        [SerializeField] private StateHolderDefinition _stateHolderDefinition;

        [ReadOnly][ShowInInspector] private Dictionary<Type, AState> _states;

        public string Name { get; private set; }

        public bool CheckStateExists(Type stateType)
        {
            return _stateHolderDefinition.StateInfos.FirstOrDefault(x => x.StateType.Name == stateType.Name) != null;
        }

        public TState TryGetState<TState>()
            where TState : AState
        {
            var stateInfo = _stateHolderDefinition.StateInfos
                .FirstOrDefault(x => x.StateType == typeof(TState));

            if (stateInfo == null) return null;
            _states ??= new Dictionary<Type, AState>();

            if (!_states.ContainsKey(typeof(TState)))
                _states.Add(typeof(TState), stateInfo.CreateNewState());

            return _states[typeof(TState)] as TState;

        }

        private void Awake()
        {
            Name = _stateHolderDefinition.Info.Name;
        }

        private void Start()
        {
            if (_states == null || !_states.Any()) return;
            foreach (var state in _states) state.Value.InvokeAll();
        }
    }
}
