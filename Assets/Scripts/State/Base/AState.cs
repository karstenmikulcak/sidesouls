﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WickedGames
{
    public abstract class AState
    {
        public abstract void InvokeAll();
    }
}
