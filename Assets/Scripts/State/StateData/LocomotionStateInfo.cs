﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace WickedGames
{
    [CreateAssetMenu(fileName = "LocomotionStateInfo_", menuName = "StateHolder/StateInfo/Locomotion")]
    public class LocomotionStateInfo : AStateInfo
    {
        public float RunSpeed;
        public float WalkSpeed;
        public bool AddForward;

        public override Type StateType => typeof(LocomotionState);

        public override AState CreateNewState()
        {
            return new LocomotionState(this);
        }
    }
}
