﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;

namespace WickedGames
{

    public class GameManager : AManager<GameManager>
    {
        private bool _waitForSpawn;

        //[SerializeField] private GameObject _cameraPrefab;

        [ReadOnly][ShowInInspector] private Player _player;

        [HideInInspector] public OnPlayerJoined OnPlayerJoinedCallback;
        [HideInInspector] public delegate void OnPlayerJoined(Player player);

        public Player Player
        {
            get
            {
                if (_player == null && !_waitForSpawn)
                {
                    var player = GameObject.FindWithTag("Player");
                    if (player == null) _waitForSpawn = true;
                    else
                    {
                        _player = player.GetComponent<Player>();
                        _waitForSpawn = false;
                        OnPlayerJoinedCallback?.Invoke(_player);
                    }

                    Debug.Log($"TryToGetPlayer waitForSpawn:{_waitForSpawn}");
                }
                return _player;
            }
        }

        internal void PlayerCreated(Player player)
        {
            _player = player;
            OnPlayerJoinedCallback?.Invoke(_player);

            if (Camera.main == null)
            {
                //Instantiate(_cameraPrefab);
            }
        }
    }
}
