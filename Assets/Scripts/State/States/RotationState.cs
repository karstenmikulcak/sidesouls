﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;

namespace WickedGames
{
    [Serializable]
    public class RotationState : AState
    {
        private bool _isFacingForward;
        private bool _isRotating;
        private float _currentRotationSpeed;
        private float _currentAnimationRotationSpeed;

        [HideInInspector]
        public delegate void OnRotationStateDirectionChange(bool isFacingForward);
        [HideInInspector]
        public delegate void OnRotationStateIsRotatingChange(bool isRotating);
        [HideInInspector]
        public delegate void OnRotationStateSpeedChange(float rotationSpeed);
        [HideInInspector]
        public delegate void OnRotationStateAnimationSpeedChange(float animationRotationSpeed);

        [HideInInspector] 
        public OnRotationStateDirectionChange OnRotationStateDirectionChangeCallback;
        [HideInInspector] 
        public OnRotationStateIsRotatingChange OnRotationStateIsRotatingChangeCallback;
        [HideInInspector] 
        public OnRotationStateSpeedChange OnRotationStateSpeedChangeCallback;
        [HideInInspector] 
        public OnRotationStateAnimationSpeedChange OnRotationStateAnimationSpeedChangeCallback;

        public RotationState(RotationStateInfo rotationInfo)
        {
            _currentRotationSpeed = rotationInfo.RotationSpeed;
            _currentAnimationRotationSpeed = rotationInfo.RotationAnimationSpeed;
            IsFacingForward = rotationInfo.IsFacingForward;
        }

        [ShowInInspector]
        public bool IsFacingForward
        {
            get => _isFacingForward;
            private set
            {
                _isFacingForward = value;
                OnRotationStateDirectionChangeCallback?.Invoke(_isFacingForward);
            }
        }

        [ShowInInspector]
        public bool IsRotating
        {
            get => _isRotating;
            private set
            {
                _isRotating = value;
                OnRotationStateIsRotatingChangeCallback?.Invoke(_isRotating);
            }
        }


        [ShowInInspector]
        public float CurrentRotationSpeed
        {
            get => _currentRotationSpeed;
            private set
            {
                _currentRotationSpeed = value;
                OnRotationStateSpeedChangeCallback?.Invoke(_currentRotationSpeed);
            }
        }

        [ShowInInspector]
        public float CurrentAnimationRotationSpeed
        {
            get => _currentAnimationRotationSpeed;
            private set
            {
                _currentAnimationRotationSpeed = value;
                OnRotationStateAnimationSpeedChangeCallback?.Invoke(_currentAnimationRotationSpeed);
            }
        }

        public void UpdateRotation(Vector2 direction)
        {
            if (direction == Vector2.zero) return;
            var rotation = direction.x >= 1;
            if (_isFacingForward != rotation) IsFacingForward = rotation;
        }

        public void UpdateIsRotating(bool isRotating)
        {
            if (_isRotating != isRotating) IsRotating = isRotating;
        }

        public void IncreaseRotationSpeed(float amount)
        {
            var newAmount = _currentRotationSpeed + amount;
            ChangeRotationSpeed(newAmount);
        }

        public void DecreaseRotationSpeed(float amount)
        {
            var newAmount = _currentRotationSpeed - amount;
            ChangeRotationSpeed(newAmount);
        }

        private void ChangeRotationSpeed(float amount)
        {
            if (_currentRotationSpeed != amount) CurrentRotationSpeed = amount;
        }

        public void IncreaseAnimationRotationSpeed(float amount)
        {
            var newAmount = _currentRotationSpeed + amount;
            ChangeAnimationRotationSpeed(newAmount);
        }

        public void DecreaseAnimationRotationSpeed(float amount)
        {
            var newAmount = _currentRotationSpeed - amount;
            ChangeAnimationRotationSpeed(newAmount);
        }

        private void ChangeAnimationRotationSpeed(float amount)
        {
            if (_currentAnimationRotationSpeed != amount) CurrentAnimationRotationSpeed = amount;
        }

        public override void InvokeAll()
        {
            OnRotationStateSpeedChangeCallback?.Invoke(CurrentRotationSpeed);
        }
    }
}
