﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace WickedGames
{
    public abstract class StateConnector : MonoBehaviour
    {
        protected StateHolder StateHolder;
        protected abstract Type[] StatesRequired { get; }

        private void Awake()
        {
            var run = true;

            StateHolder = GetComponent<StateHolder>();
            if (StateHolder == null)
                StateHolder = GetComponentInParent<StateHolder>();

            if (StateHolder != null)
            {
                if (StatesRequired != null)
                    foreach (var state in StatesRequired)
                    {
                        run = StateHolder.CheckStateExists(state);
                        if (!run) break;
                    }
            }
            else run = false;

            if (run) OnAwake();
            else gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            SubscribeToStates();
        }

        private void OnDisable()
        {
            UnSubscribeToStates();
        }

        protected abstract void OnAwake();

        protected abstract void SubscribeToStates();

        protected abstract void UnSubscribeToStates();
    }
}
