using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace WickedGames
{
    public class PlayerMovement : StateConnector
    {
        private const float MOVE_SPEED_MULTIPLIER = 50;

        private float _speed;
        private Vector3 _targetPosition = Vector3.zero;

        [SerializeField] private CharacterController _characterController;
        [ReadOnly][ShowInInspector] private LocomotionState _locomotionState;

        protected override Type[] StatesRequired => new[] { typeof(LocomotionState) };

        protected override void OnAwake()
        {
            _locomotionState = StateHolder.TryGetState<LocomotionState>();
        }

        private void FixedUpdate()
        {
            _characterController.SimpleMove(_targetPosition.normalized * _speed * Time.fixedDeltaTime);
        }

        private void HandleMoveDirection(Vector2 direction)
        {
            _targetPosition = new Vector3(0, 0, 0);

            if (!_characterController.isGrounded) return;
            if (direction.x == 0) return;

            if (_locomotionState.AddForward)
                _targetPosition += _characterController.transform.forward;
            else
            {
                if (direction.x > 0) _targetPosition += _characterController.transform.forward;
                if (direction.x < 0) _targetPosition -= _characterController.transform.forward;
            }
        }

        private void HandleMovementSpeed(float movementSpeed)
        {
            _speed = movementSpeed * MOVE_SPEED_MULTIPLIER;
        }

        protected override void SubscribeToStates()
        {
            _locomotionState.OnLocomotionStateDirectionChangeCallback += HandleMoveDirection;
            _locomotionState.OnMovementSpeedChangeCallback += HandleMovementSpeed;
        }

        protected override void UnSubscribeToStates()
        {
            _locomotionState.OnLocomotionStateDirectionChangeCallback -= HandleMoveDirection;
            _locomotionState.OnMovementSpeedChangeCallback -= HandleMovementSpeed;
        }
    }
}
