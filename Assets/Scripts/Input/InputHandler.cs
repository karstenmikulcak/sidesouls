using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace WickedGames
{
    public class InputHandler : MonoBehaviour
    {
        private enum InputActions
        {
            Movement,
            Run,
            Jump
        }

        private Vector2 _movement;
        private PlayerInput _playerInput;
        private VirtualInputManager _virtualInputManager;

        private InputAction _movementAction;
        private InputAction _runAction;
        private InputAction _jumpAction;


        private void Awake()
        {
            _playerInput = GetComponent<PlayerInput>();
            _virtualInputManager = VirtualInputManager.Instance;

            _movementAction = _playerInput.actions[InputActions.Movement.ToString()];
            _runAction = _playerInput.actions[InputActions.Run.ToString()];
            _jumpAction = _playerInput.actions[InputActions.Jump.ToString()];
        }

        private void Start()
        {
            _runAction.started += _ => Run(true);
            _runAction.canceled += _ => Run(false);

            _jumpAction.started += _ => Jump(true);
            _jumpAction.canceled += _ => Jump(false);
        }

        private void OnDisable()
        {
            _runAction.started -= _ => Run(true);
            _runAction.canceled -= _ => Run(false);

            _jumpAction.started -= _ => Jump(true);
            _jumpAction.canceled -= _ => Jump(false);
        }

        private void Update()
        {
            _movement = _movementAction.ReadValue<Vector2>();
        }

        private void FixedUpdate()
        {
            _virtualInputManager.PlayerInput.Movement = _movement;
        }

        private void Run(bool run)
        {
            _virtualInputManager.PlayerInput.Run = run;
        }

        private void Jump(bool jump)
        {
            _virtualInputManager.PlayerInput.Jump = jump;
        }
    }
}
